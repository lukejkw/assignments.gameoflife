﻿using GameOfLife.Business;
using GameOfLife.Data;
using GameOfLife.Data.Domains;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Script.Serialization;

namespace GameOfLife.Mvc.Models
{
    public class GameViewModel : CellsViewModelBase
    {
        #region Properties

        private List<UserTemplate> _userTemplates;

        [ScriptIgnore]
        public List<UserTemplate> UserTemplates
        {
            get
            {
                if (_userTemplates == null)
                {
                    using (var unit = new UnitOfWork(GOLContext.Create()))
                    {
                        _userTemplates = unit.UserTemplates.GetAll().ToList();
                    }
                }
                return _userTemplates;
            }
            set
            {
                _userTemplates = value;
            }
        }

        [Required]
        [Display(Name = "User Template")]
        public int UserTemplateId { get; set; }

        [Required]
        [Display(Name = "Position X")]
        [Range(0, int.MaxValue)]
        public int TemplateX { get; set; }

        [Required]
        [Display(Name = "Position Y")]
        [Range(0, int.MaxValue)]
        public int TemplateY { get; set; }

        public override bool IsValid
        {
            get
            {
                if (!base.IsValid ||
                    TemplateX <= 0 ||
                    TemplateY < 0)
                    return false;

                return true;
            }
        }

        public UserGame CurrentUserGameState
        {
            get
            {
                return new UserGame()
                {
                    Name = Name,
                    Height = Height,
                    Width = Width,
                    Cells = Cells
                };
            }
        }

        #endregion

        #region Ctors

        public GameViewModel()
        {
        }

        public GameViewModel(UserGame game) : this()
        {
            Id = game.Id;
            Height = game.Height;
            Width = game.Width;
            Cells = game.Cells;
            Name = game.Name;
            UserId = game.UserId;
        }

        #endregion

        #region Methods

        public void InsertTemplateIntoGame(UserTemplate template)
        {
            // Get current game state and insert
            var game = CurrentUserGameState;
            game.InsertTemplate(template, TemplateX, TemplateY);
            Cells = game.Cells;
        }

        #endregion
    }
}