﻿using System.Collections.Generic;

namespace GameOfLife.Mvc.Models
{
    public class GameListViewModel
    {
        public List<GameViewModel> Games { get; set; }
    }
}