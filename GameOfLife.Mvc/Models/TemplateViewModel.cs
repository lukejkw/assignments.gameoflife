﻿using GameOfLife.Data;
using GameOfLife.Data.Domains;
using GameOfLife.Data.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GameOfLife.Mvc.Models
{
    public class TemplateViewModel : CellsViewModelBase
    {
        #region Public Properties

        public string CreateBy
        {
            get
            {
                var user = ApplicationUserManager.GetAll().SingleOrDefault(u => u.Id == UserId);
                return user != null ? user.FullName : string.Empty;
            }
        }

        public override bool IsValid
        {
            get
            {
                // Check that view model is valid
                if (!base.IsValid)
                    return false;
                if (string.IsNullOrEmpty(Name))
                    return false;

                return true;
            }
        }

        [Required]
        public override string Cells { get; set; }

        #endregion

        #region Ctors

        public TemplateViewModel()
        { }

        public TemplateViewModel(string userId)
        {
            UserId = userId;
        }

        public TemplateViewModel(UserTemplate template)
        {
            Id = template.Id;
            Name = template.Name;
            UserId = template.UserId;
            Height = template.Height;
            Width = template.Width;
            Cells = template.Cells;
        }

        #endregion

        #region Public Methods

        public UserTemplate UserTemplate
        {
            get
            {
                // Construct template obj
                var template = new UserTemplate()
                {
                    Id = Id,
                    Cells = Cells,
                    Height = Height,
                    Width = Width,
                    Name = Name
                };
                template.UserId = UserId;
                return template;
            }
        }

        public void SaveTemplate()
        {
            // Persist
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                uow.UserTemplates.AddOrUpdate(UserTemplate);
                uow.Complete();
            }
        }

        public void DeleteTemplate()
        {
            // Persist
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                var template = uow.UserTemplates.SingleOrDefault(t => t.Id == Id);
                if (template != null)
                {
                    uow.UserTemplates.Remove(template);
                    uow.Complete();
                }
            }
        }

        #endregion
    }
}