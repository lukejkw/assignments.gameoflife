﻿using System.Collections.Generic;

namespace GameOfLife.Mvc.Models
{
    public class TemplatesListViewModel
    {
        public bool UserFilter { get; set; }

        public string Filter { get; set; }

        public List<TemplateViewModel> Templates { get; set; }

        public TemplatesListViewModel(List<TemplateViewModel> templates)
        {
            Templates = templates;
        }

        public TemplatesListViewModel()
        {
            Templates = new List<TemplateViewModel>();
        }
    }
}