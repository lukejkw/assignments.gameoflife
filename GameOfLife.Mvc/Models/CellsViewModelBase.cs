﻿using GameOfLife.Business;
using System;
using System.ComponentModel.DataAnnotations;

namespace GameOfLife.Mvc.Models
{
    public abstract class CellsViewModelBase
    {
        public int Id { get; set; } = -1;

        public string UserId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Width { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Height { get; set; }

        public virtual string Cells { get; set; }

        /// <summary>
        /// Turns X & 0 based cells to the Console based look
        /// </summary>
        public string CellsForDisplay
        {
            get
            {
                return Utilities.GetDisplayCellsFromDbCells(Cells);
            }
        }

        public virtual bool IsValid
        {
            get
            {
                try
                {
                    // Try convert
                    var cells = Cells.ToCellsArray(Height, Width);
                    return true;
                }
                catch
                {
                    // Could not convert to cells array
                    return false;
                }
            }
        }
    }
}