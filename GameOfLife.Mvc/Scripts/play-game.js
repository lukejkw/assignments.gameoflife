﻿$(function () {
    // Vars
    var $btnStart = $('#btnStart');
    var $btnStop = $('#btnStop');
    var $cells = $('#cells-container');
    var gameName = $('#hdnName').val();
    var gameJob;

    // Events
    $btnStart.click(togglePlay);
    $btnStop.click(togglePlay);

    // Functions
    function togglePlay() {
        if (!gameJob) {
            // Start game job to run every second
            gameJob = setInterval(gameTick, 1000);
        }
        else {
            clearInterval(gameJob);
        }
        toggleButtons();
    }
    function toggleButtons() {
        $btnStart.toggleClass('disabled');
        $btnStop.toggleClass('disabled');
    }
    function gameTick() {
        $.ajax({
            type: "GET",
            cache: false,
            url: "/UserGame/GetUpdatedGameCells",
            data: {
                "gameName": gameName
            }
        }).done(function (data) {
            // AJAX call was a success
            if (typeof data !== 'undefined' && data !== null) {
                // We have data! show user
                $cells.text(data.CellsForDisplay);
            }
        }).fail(function (xhr, status) {
            console.log('Call to server failed' + status);
        });
    }
});