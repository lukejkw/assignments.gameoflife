namespace GameOfLife.Mvc.Migrations
{
    using Data;
    using Data.Security;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity.Migrations;
    using System.Diagnostics;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<GOLContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(GOLContext context)
        {
            // Seed role
            if (!context.Roles.Any(r => r.Name == RoleKeys.ADMIN))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = RoleKeys.ADMIN };
                manager.Create(role);
            }

            // Make sure that our admin user exists
            var user = new ApplicationUser()
            {
                First = "Superuser",
                Last = "Account",
                UserName = "admin@gmail.com",
                Email = "admin@gmail.com"
            };

            if (!context.Users.Any(u => u.UserName == user.Email))
            {
                // Init user manager and create admin
                var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
                var result = manager.Create(user, "rmit");
                if (!result.Succeeded)
                    throw new Exception($"Could not create user {result.Errors.First()}");
                manager.AddToRole(user.Id, RoleKeys.ADMIN);

                // Create normal users
                user = new ApplicationUser()
                {
                    First = "Luke",
                    Last = "Warren",
                    UserName = "lukejkw@gmail.com",
                    Email = "lukejkw@gmail.com"
                };
                manager.Create(user, "z00m@#$");
            }
        }
    }
}