﻿using GameOfLife.Data;
using GameOfLife.Data.Security;
using System.Web.Mvc;

namespace GameOfLife.Mvc.Controllers
{
    public abstract class BaseController : Controller
    {
        #region Members

        private ApplicationUser _applicationUser;

        #endregion

        #region Properties

        protected ApplicationUser ApplicationUser
        {
            get
            {
                return _applicationUser = _applicationUser ?? User.ToApplicationUser();
            }
        }

        #endregion

        #region Methods

        protected virtual string ErrorMessage
        {
            get
            {
                return ViewBag.ErrorMessage;
            }
            set
            {
                ViewBag.ErrorMessage = value;
            }
        }

        protected virtual string SuccessMessage
        {
            get
            {
                return ViewBag.SuccessMessage;
            }
            set
            {
                ViewBag.SuccessMessage = value;
            }
        }

        #endregion
    }
}