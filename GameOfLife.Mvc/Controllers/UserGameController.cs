﻿using GameOfLife.Business;
using GameOfLife.Data;
using GameOfLife.Mvc.Contracts;
using GameOfLife.Mvc.Models;
using GameOfLife.Mvc.Services;
using System;
using System.Linq;
using System.Web.Mvc;

namespace GameOfLife.Mvc.Controllers
{
    public class UserGameController : BaseController
    {
        #region Members

        private IActiveGameService _service;

        #endregion

        #region Ctors

        public UserGameController(IActiveGameService service)
        {
            _service = service;
        }

        public UserGameController() : this(new ActiveGameService())
        {
        }

        #endregion

        #region Actions

        #region Anonymous

        public ActionResult Index()
        {
            var games = _service.ActiveGames.Select(g => new GameViewModel(g)).ToList();
            var model = new GameListViewModel()
            {
                Games = games
            };
            return View("Games", model);
        }

        public ActionResult CreateGame(int templateId = -1)
        {
            var model = new GameViewModel();
            using (var unit = new UnitOfWork(GOLContext.Create()))
                model.UserTemplates = unit.UserTemplates.GetAll().ToList();
            model.UserTemplateId = templateId;
            return View("Create", model);
        }

        public ActionResult MakeActive(int id)
        {
            using (var unit = new UnitOfWork(GOLContext.Create()))
            {
                var game = unit.UserGames.Get(id);
                if (game == null)
                {
                    ErrorMessage = "Unable to find game";
                    return RedirectToAction("Saved");
                }
                var model = new GameViewModel(game);
                return AddToActiveAndRedirect(model);
            }
        }

        [HttpPost]
        public ActionResult Create(GameViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var uow = new UnitOfWork(GOLContext.Create()))
                    {
                        // Get template and insert into model
                        var template = uow.UserTemplates.Get(model.UserTemplateId);
                        model.InsertTemplateIntoGame(template);
                    }

                    return AddToActiveAndRedirect(model);
                }
            }
            catch (Exception ex)
            {
                // Something happened when trying to create the game
                ErrorMessage = $"Invalid game. Message: {ex.Message}";
            }
            // Just return the create view
            return View("Create", model);
        }

        public ActionResult PlayGame(string gameName)
        {
            var game = _service.ActiveGames.SingleOrDefault(g => g.Name == gameName);
            if (game == null)
            {
                ErrorMessage = $"Unable to find game by name: {gameName}";
                RedirectToAction("Index");
            }

            var model = new GameViewModel(game);
            return View("Play", model);
        }

        public ActionResult GetUpdatedGameCells(string gameName)
        {
            // Get game
            var game = _service.ActiveGames.SingleOrDefault(g => g.Name == gameName);
            if (game == null)
                return Json(new { CellsForDisplay = "Error" }, JsonRequestBehavior.AllowGet);

            // Tick it over and save it back to session
            game.Tick();
            _service.AddOrUpdateActiveGame(game);

            // Return model for display
            var model = new GameViewModel(game);
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteActiveGame(string gameName)
        {
            _service.DeleteGame(gameName);
            return RedirectToAction("Index");
        }

        #endregion

        #region Authorised

        [Authorize]
        public ActionResult Saved()
        {
            using (var unit = new UnitOfWork(GOLContext.Create()))
            {
                var games = unit.UserGames.GetAll()
                    .Where(g => g.UserId == ApplicationUser.Id)
                    .Select(g => new GameViewModel(g))
                    .ToList();
                var model = new GameListViewModel()
                {
                    Games = games
                };
                return View("Saved", model);
            }
        }

        [Authorize]
        public ActionResult SaveGame(string gameName)
        {
            // Get game
            var game = _service.ActiveGames.SingleOrDefault(g => g.Name == gameName);
            if (game == null)
            {
                ErrorMessage = $"Unable to find game by name: {gameName}";
                RedirectToAction("Index");
            }

            // Persist to db as saved game
            game.UserId = ApplicationUser.Id;
            using (var unit = new UnitOfWork(GOLContext.Create()))
            {
                unit.UserGames.AddOrUpdate(game);
                unit.Complete();
                SuccessMessage = "Game Saved";
            }

            // Show play view
            return View("Play", new GameViewModel(game));
        }

        [Authorize]
        public ActionResult DeleteSavedGame(int id)
        {
            using (var unit = new UnitOfWork(GOLContext.Create()))
            {
                var game = unit.UserGames.Get(id);
                if (game != null)
                {
                    // Check that user has permissions to delete
                    if (game.UserId != ApplicationUser.Id && !User.IsInRole(RoleKeys.ADMIN))
                    {
                        ErrorMessage = "You cannot delete a game that isn't yours";
                        var games = unit.UserGames.GetAll()
                                                  .Where(g => g.UserId == ApplicationUser.Id)
                                                  .Select(g => new GameViewModel(g))
                                                  .ToList();
                        var model = new GameListViewModel()
                        {
                            Games = games
                        };
                        return View("Saved", model);
                    }
                    else
                    {
                        // Game exists and user authorised
                        unit.UserGames.Remove(game);
                        unit.Complete();
                        SuccessMessage = "Game Deleted";
                    }
                }
            }
            return RedirectToAction("Saved");
        }

        #endregion

        #endregion

        #region Helpers

        private RedirectToRouteResult AddToActiveAndRedirect(GameViewModel model)
        {
            // Since we are using the name to find games, ensure unique
            if (_service.ActiveGames.Any(g => g.Name == model.Name))
                model.Name += "2";

            // Add current game state to session and display play game view
            _service.AddOrUpdateActiveGame(model.CurrentUserGameState);
            return RedirectToAction("PlayGame", new { gameName = model.Name });
        }

        #endregion
    }
}