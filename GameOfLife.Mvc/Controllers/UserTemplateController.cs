﻿using GameOfLife.Data;
using GameOfLife.Mvc.Models;
using System.Linq;
using System.Web.Mvc;

namespace GameOfLife.Mvc.Controllers
{
    /// <summary>
    /// Responsible for all UserTemplate Interactions
    /// </summary>
    public class UserTemplateController : BaseController
    {
        #region Actions

        #region Anonymous

        public ActionResult Index(bool userOnly = false)
        {
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                var templateModels = uow.UserTemplates.GetAll()
                                                      .Select(t => new TemplateViewModel(t))
                                                      .ToList();
                var model = new TemplatesListViewModel(templateModels);
                return View("Templates", model);
            }
        }

        public ActionResult Details(int id)
        {
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                // Try get model
                var template = uow.UserTemplates.SingleOrDefault(t => t.Id == id);
                if (template == null)
                {
                    // If we could not find the template, redirect
                    ErrorMessage = "Oops... Unable to find template.";
                    var templates = uow.UserTemplates.GetAll().Select(t => new TemplateViewModel(t)).ToList();
                    return View("Templates", new TemplatesListViewModel(templates));
                }
                return View("Details", new TemplateViewModel(template));
            }
        }

        [HttpPost]
        public ActionResult Search(TemplatesListViewModel model)
        {
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                // Get filtered list of templates
                model.Templates = uow.UserTemplates.Find(t => t.Name.ToUpper().Contains(model.Filter.ToUpper()))
                    .Select(t => new TemplateViewModel(t))
                    .ToList();
                return View("Templates", model);
            }
        }

        #endregion

        #region Authorised

        [Authorize]
        public ActionResult MyTemplates()
        {
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                var templateModels = uow.UserTemplates.GetAll()
                                                      .Where(t => t.UserId == ApplicationUser.Id)
                                                      .Select(t => new TemplateViewModel(t))
                                                      .ToList();
                var model = new TemplatesListViewModel(templateModels);
                return View("UserTemplates", model);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult SearchMyTemplates(TemplatesListViewModel model)
        {
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                // Get filtered list of templates
                model.Templates = uow.UserTemplates.GetAll()
                                                   .Where(t => t.UserId == User.ToApplicationUser().Id && t.Name.ToUpper().Contains(model.Filter.ToUpper()))
                                                   .Select(t => new TemplateViewModel(t))
                                                   .ToList();
                return View("UserTemplates", model);
            }
        }

        [Authorize]
        public ActionResult Edit(int? id)
        {
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                // Try get template and populate model
                var template = uow.UserTemplates.SingleOrDefault(t => t.Id == id);

                TemplateViewModel model;
                if (template == null)
                {
                    model = new TemplateViewModel(ApplicationUser?.Id);
                }
                else
                {
                    // Make sure that user is authorised
                    if (template.UserId != ApplicationUser.Id && !User.IsInRole(RoleKeys.ADMIN))
                    {
                        ErrorMessage = "That was naughty! You are not allowed to edit that template";
                        var templates = uow.UserTemplates.GetAll()
                                                         .Select(t => new TemplateViewModel(t)).ToList();
                        return View("Templates", new TemplatesListViewModel(templates));
                    }

                    // User is authorised
                    model = new TemplateViewModel(template);
                }

                return View("Edit", model);
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult Edit(TemplateViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Check for errors
                string error = string.Empty;
                if (!model.IsValid)
                    error = "Invalid template. Please make sure that the cells match your specified width and height";

                if (!string.IsNullOrEmpty(error))
                {
                    // Last error wins
                    ErrorMessage = error;
                    return View("Edit", model);
                }

                // Persist
                using (var uow = new UnitOfWork(GOLContext.Create()))
                {
                    // Try Retrieve
                    var template = uow.UserTemplates.SingleOrDefault(t => t.Id == model.Id);
                    if (template == null)
                    {
                        // Does not exit so lets just add it
                        template = model.UserTemplate;
                        template.UserId = ApplicationUser?.Id;
                    }
                    else
                    {
                        // It's an update, so update the object already in tracking
                        template.Name = model.Name;
                        template.Height = model.Height;
                        template.Width = model.Width;
                        template.Cells = model.Cells;
                        template.UserId = ApplicationUser?.Id;
                    }
                    uow.UserTemplates.AddOrUpdate(template);
                    uow.Complete();
                    SuccessMessage = $"Template {template.Name} successfully saved";
                }
            }
            return View("Edit", model);
        }

        [Authorize]
        public ActionResult Delete(int id)
        {
            using (var uow = new UnitOfWork(GOLContext.Create()))
            {
                // Try get model
                var template = uow.UserTemplates.SingleOrDefault(t => t.Id == id);
                if (template == null)
                {
                    // If we could not find the template, redirect
                    ErrorMessage = "Oops... Unable to find template.";
                    var templates = uow.UserTemplates.GetAll().Select(t => new TemplateViewModel(t)).ToList();
                    return View("Templates", new TemplatesListViewModel(templates));
                }

                var model = new TemplateViewModel(template);
                model.DeleteTemplate();

                return RedirectToAction("Index", true);
            }
        }

        #endregion

        #endregion
    }
}