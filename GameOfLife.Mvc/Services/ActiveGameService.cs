﻿using GameOfLife.Data.Domains;
using GameOfLife.Mvc.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GameOfLife.Mvc.Services
{
    /// <summary>
    /// Handles dealing with Active Games in session
    /// </summary>
    public class ActiveGameService : IActiveGameService
    {
        #region Private Members

        private const string ACTIVE_GAMES_KEY = "ACTIVE_GAMES";
        private HttpContext _context;

        #endregion

        #region Properties

        public List<UserGame> ActiveGames
        {
            get
            {
                var games = HttpContext.Current.Session[ACTIVE_GAMES_KEY] as List<UserGame>;
                if (null == games)
                {
                    games = new List<UserGame>();
                    HttpContext.Current.Session[ACTIVE_GAMES_KEY] = games;
                }
                return games;
            }
            private set
            {
                // Never set active games to null
                List<UserGame> valueToSet = (value == null) ? new List<UserGame>() : value;
                HttpContext.Current.Session[ACTIVE_GAMES_KEY] = valueToSet;
            }
        }

        #endregion

        #region Ctors

        public ActiveGameService(HttpContext context)
        {
            _context = context;
        }

        public ActiveGameService() : this(HttpContext.Current)
        { }

        #endregion

        #region Methods

        public void AddOrUpdateActiveGame(UserGame game)
        {
            if (game == null)
                throw new ArgumentNullException("Cannot add/update game that is null");

            var activeGames = ActiveGames;

            var existingGame = activeGames.SingleOrDefault(g => g.Name == game.Name);
            if (existingGame != null)
            {
                existingGame.Height = game.Height;
                existingGame.Width = game.Width;
                existingGame.Cells = game.Cells;
                existingGame.CreatedOnDate = game.CreatedOnDate;
                existingGame.LastChangedOnDate = DateTime.Now;
            }
            else
            {
                activeGames.Add(game);
            }
            ActiveGames = activeGames;
        }

        public void DeleteGame(string gameName)
        {
            var games = ActiveGames;
            var toDelete = games.SingleOrDefault(g => g.Name == gameName);
            if (toDelete != null)
            {
                games.Remove(toDelete);
                ActiveGames = games;
            }
        }

        #endregion
    }
}