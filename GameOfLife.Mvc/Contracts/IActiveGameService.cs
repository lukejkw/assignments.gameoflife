﻿using GameOfLife.Data.Domains;
using System.Collections.Generic;

namespace GameOfLife.Mvc.Contracts
{
    public interface IActiveGameService
    {
        List<UserGame> ActiveGames { get; }

        void AddOrUpdateActiveGame(UserGame game);

        void DeleteGame(string gameName);
    }
}