﻿CREATE PROCEDURE [dbo].[UserGames_Delete]
	@Id INT
AS
BEGIN
	DELETE
	FROM 
		dbo.UserGames
	WHERE
		[Id] = @Id
END
