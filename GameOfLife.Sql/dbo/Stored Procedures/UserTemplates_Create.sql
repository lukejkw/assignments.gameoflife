﻿CREATE PROCEDURE [dbo].[UserTemplates_Create]
	@Height int,
	@Width int,
	@Cells NVARCHAR(MAX),
	@Name NVARCHAR(MAX),
	@UserId NVARCHAR(128)
AS
BEGIN
	INSERT
	INTO
		dbo.UserTemplates
	(
		[Height],
		[Width],
		[Cells],
		[Name],
		[UserId],
		[CreatedOnDate],
		[LastChangedOnDate]
	)
	VALUES
	(
		@Height,
		@Width,
		@Cells,
		@Name,
		@UserId,
		GETDATE(),
		GETDATE()
	)
END
