﻿CREATE PROCEDURE [dbo].[AspNetUsers_Delete]
	@UserId NVARCHAR(128)
AS
BEGIN
	-- Delete Claims
	DELETE
	FROM
		dbo.AspNetUserClaims
	WHERE
		[UserId] = @UserId

	-- Delete Logins
	DELETE
	FROM
		dbo.AspNetUserLogins
	WHERE
		[UserId] = @UserId

	-- Roles
	DELETE
	FROM
		dbo.AspNetUserRoles
	WHERE
		[UserId] = @UserId


	-- Delete user game
	DELETE
	FROM
		dbo.UserGames
	WHERE
		[UserId] = @UserId

	-- Delete user templates
	DELETE
	FROM
		dbo.UserTemplates
	WHERE
		[UserId] = @UserId


	-- Finally, delete the user
	DELETE
	FROM
		dbo.AspNetUsers
	WHERE
		[Id] = @UserId
END
