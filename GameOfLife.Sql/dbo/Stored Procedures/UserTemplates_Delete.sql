﻿CREATE PROCEDURE [dbo].[UserTemplates_Delete]
	@Id INT
AS
BEGIN
	DELETE
	FROM 
		dbo.UserTemplates
	WHERE
		[Id] = @Id
END
