﻿CREATE TABLE [dbo].[UserTemplates] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [UserId]            NVARCHAR (128) NULL,
    [Name]              NVARCHAR (MAX) NOT NULL,
    [Height]            INT            NOT NULL,
    [Width]             INT            NOT NULL,
    [Cells]             NVARCHAR (MAX) NULL,
    [CreatedOnDate]     DATETIME       NOT NULL,
    [LastChangedOnDate] DATETIME       NOT NULL,
    CONSTRAINT [PK_dbo.UserTemplates] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.UserTemplates_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserTemplates]([UserId] ASC);

