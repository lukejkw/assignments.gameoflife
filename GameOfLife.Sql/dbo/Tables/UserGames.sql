﻿CREATE TABLE [dbo].[UserGames] (
    [Id]                INT            IDENTITY (1, 1) NOT NULL,
    [Name]              NVARCHAR (MAX) NOT NULL,
    [Height]            INT            NOT NULL,
    [Width]             INT            NOT NULL,
    [Cells]             NVARCHAR (MAX) NULL,
    [UserId]            NVARCHAR (128) NULL,
    [CreatedOnDate]     DATETIME       NOT NULL,
    [LastChangedOnDate] DATETIME       NOT NULL,
    CONSTRAINT [PK_dbo.UserGames] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_dbo.UserGames_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_UserId]
    ON [dbo].[UserGames]([UserId] ASC);

