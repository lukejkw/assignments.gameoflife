﻿using GameOfLife.Common;
using GameOfLife.Properties;
using GameOfLife.Utilities;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife.Components
{
    public static class GameOfLifeExtensions
    {
        #region Public Extension Methods

        /// <summary>
        /// Removes starting template dimesions and replaces dead and live cells with the appropriate console representaion
        /// </summary>
        /// <param name="templateString">The string to convert</param>
        /// <returns>ConsoleString</returns>
        public static string ToGamifiedString(this string templateString)
        {
            return templateString.Substring(4) // Remove first 2 lines (game size lines)
                                 .Replace(Settings.Default.CellAlive, Constants.ALIVE_CELL_CHAR)
                                 .Replace(Settings.Default.CellDead, Constants.DEAD_CELL_CHAR);
        }

        /// <summary>
        /// Starts a game
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        public static async Task StartGameAsync(this GameOfLife game)
        {
            // Init job runner to run jobs every 1 second
            var cancelSource = new CancellationTokenSource();
            var job = new JobRunner(cancelSource.Token, TimeSpan.FromSeconds(1));

            // Subscribe to events and start job
            job.OnJobIteration += () =>
            {
                Console.Clear();
                game.TakeTurn();
                Console.WriteLine(game.ToString());
                Console.WriteLine($"Press any key to stop the game (game running for {job.IterationCount} seconds)");
            };
            job.OnJobEnd += () =>
            {
                game.SaveObjectState(Settings.Default.SavedGameName);
                Console.WriteLine("Game stopped - game state has been saved");
            };
            var task = job.StartAsync();

            // Block current thread - only continue when user hits a key
            Console.ReadKey(true);
            cancelSource.Cancel();
            await task;
        }

        #endregion
    }
}