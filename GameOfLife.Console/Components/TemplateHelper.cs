﻿using GameOfLife.Common;
using GameOfLife.Contracts;
using GameOfLife.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using static System.Console;

namespace GameOfLife.Components
{
    /// <summary>
    /// A helper wrapper class to make dealing with Templates easier
    /// </summary>
    public class TemplateHelper : PromptBase
    {
        #region Private members

        private List<string> _templates;
        private string _templateDirectory;

        #endregion

        #region Properties

        /// <summary>
        /// Gets a list of the saved template names
        /// </summary>
        public List<string> Templates
        {
            get
            {
                if (_templates == null)
                    _templates = Directory.GetFiles(TemplateDirectory)
                                          .Select(s => Path.GetFileName(s).Replace(".txt", string.Empty))
                                          .ToList();
                return _templates;
            }
        }

        public string TemplateDirectory
        {
            get
            {
                if (_templateDirectory == null)
                    _templateDirectory = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Templates\";

                if (!Directory.Exists(_templateDirectory))
                    Directory.CreateDirectory(_templateDirectory);

                return _templateDirectory;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Prompts for all the information needed to create a template and saves it to file
        /// </summary>
        /// <returns>Boolean value indicating success or failure</returns>
        public bool CreateTemplate()
        {
            // Get Game properties
            string name = PromptForString($"{Settings.Default.CreateTemplateHeader}{Environment.NewLine}{Settings.Default.TemplateNamePrompt}");
            int width = PromptForInt(Settings.Default.TemplateWidthPrompt);
            int height = PromptForInt(Settings.Default.TemplateHeightPrompt);

            // Get user input for cells
            var input = new List<string>();
            WriteLine(Settings.Default.TemplateCellsPrompt);
            for (int i = 0; i < height; i++)
            {
                string row = GetCellRow(width);

                // Add row or exit (if user elected to)
                if (!String.IsNullOrEmpty(row))
                    input.Add(row);
                else
                    return false;
            }

            // Map user input to cells array
            Cell[][] cells = new Cell[height][];
            for (int i = 0; i < height; i++)
            {
                cells[i] = new Cell[width];
                for (int j = 0; j < width; j++)
                {
                    cells[i][j] = input[i][j] == Settings.Default.CellAlive ? Cell.Alive : Cell.Dead;
                }
            }

            // Save values, notify and return
            var template = new Template(name, height, width, cells);
            Save(name, template.ToString());
            WriteLine(Settings.Default.TemplateSavedMessage);
            return true;
        }

        public Template PromptForTemplate()
        {
            WriteLine(Settings.Default.NewGameTemplates);

            // Make sure that we have templates
            if (Templates == null || Templates.Count <= 0)
            {
                WriteLine(Settings.Default.NewGameNoTemplatesFound);
                return null;
            }

            // Show templates for selection
            for (int i = 1; i <= Templates.Count; i++)
                WriteLine($"{i}. {Templates[i - 1]}");

            // Get and validate input
            int selection = PromptForInt(Settings.Default.NewGameSelectTemplate);
            if (selection < 1 || selection > Templates.Count)
            {
                WriteLine(Settings.Default.NewGameSelectionOutOfRange);
                return null;
            }

            // Open and load and display template
            string templateName = Templates[selection - 1];
            return Open(templateName);
        }

        /// <summary>
        /// Creates a Template Object Instance from a template saved on file
        /// </summary>
        /// <param name="templateName">The name of the template (do not include file extension)</param>
        /// <returns></returns>
        public Template Open(string templateName)
        {
            // Build path and ensure exists
            string path = $@"{TemplateDirectory}\{templateName}.txt";
            if (!File.Exists(path))
                return null;

            // Read lines for file and get width and height
            string[] lines = File.ReadAllLines(path);
            int height = Convert.ToInt32(lines[0]);
            int width = Convert.ToInt32(lines[1]);

            // Create cells array of arrays
            Cell[][] cells = new Cell[height][];
            for (int i = 0; i < height; i++)
            {
                cells[i] = new Cell[width];
                for (int j = 0; j < width; j++)
                {
                    cells[i][j] = lines[i + 2][j] == Settings.Default.CellAlive ? Cell.Alive : Cell.Dead;
                }
            }

            // Return template
            return new Template(templateName, height, width, cells);
        }

        /// <summary>
        /// Gets a menu string of all the saved templates
        /// </summary>
        /// <returns>String of templates</returns>
        public string GetTemplateMenuString()
        {
            var sb = new StringBuilder();
            for (int i = 0; i < Templates.Count; i++)
                sb.AppendLine($"{i + 1}. {Templates[i]}");
            return sb.ToString();
        }

        public void WriteTemplateMenu()
        {
            WriteLine(GetTemplateMenuString());
            Write(Settings.Default.TemplateListMenuPrompt);
        }

        public void SetTestableTemplateDirectory(string directory)
        {
            _templateDirectory = directory;
        }

        public GameOfLife InitGameOfLifeFromTemplate(ITemplate template)
        {
            // Get board width and height
            int height = PromptForInt(String.Format(Settings.Default.NewGameEnterHeightFormatString, template.Height));
            if (height < template.Height)
            {
                WriteLine(Settings.Default.NewGameInvalidGameDimension);
                return null;
            }
            int width = PromptForInt(String.Format(Settings.Default.NewGameEnterWidthFormatString, template.Width));
            if (width < template.Width)
            {
                WriteLine(Settings.Default.NewGameInvalidGameDimension);
                return null;
            }

            // Get x and y starting coords
            int x = PromptForInt(String.Format(Settings.Default.NewGameEnterXCoordFormatString, width - template.Width));
            if (x + template.Width > width)
            {
                WriteLine(Settings.Default.NewGameInvalidGameDimension);
                return null;
            }
            int y = PromptForInt(String.Format(Settings.Default.NewGameEnterYCoordFormatString, height - template.Height));
            if (y + template.Height > height)
            {
                WriteLine(Settings.Default.NewGameInvalidGameDimension);
                return null;
            }

            // Return hydrated game object instance
            var game = new GameOfLife(height, width);
            game.InsertTemplate(template, x, y);
            return game;
        }

        public void DisplayTemplate(ITemplate template)
        {
            WriteLine("Template");
            WriteLine($"Name: {template.Name}");
            WriteLine($"Height: {template.Height}");
            WriteLine($"Width: {template.Width}");
            Write(template.ToString().ToGamifiedString());
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Saves the template to file
        /// </summary>
        /// <param name="templateName">The name of the template (do not include '.txt')</param>
        /// <param name="templateContent">The string content to save</param>
        private void Save(string templateName, string templateContent)
        {
            string path = $"{TemplateDirectory}{templateName}.txt";
            File.WriteAllText(path, templateContent);
        }

        /// <summary>
        /// Gets a row of the template cells
        /// </summary>
        /// <param name="length">The number of cells in a row</param>
        /// <returns>The row or an empty string if the user exited</returns>
        private string GetCellRow(int length)
        {
            string row = ReadLine().Trim().ToUpper();

            if (row == "EXIT")
                return string.Empty;

            if (row.Length > length || row.Length < length)
            {
                WriteLine(Settings.Default.CellRowIncorrectLength);
                return GetCellRow(length);
            }

            foreach (char c in row)
            {
                if (c != Settings.Default.CellDead && c != Settings.Default.CellAlive)
                {
                    WriteLine(Settings.Default.CellRowContainsInvalidCharacters);
                    return GetCellRow(length);
                }
            }

            return row;
        }

        #endregion
    }
}