﻿using GameOfLife.Common;
using System;
using static System.Console;

namespace GameOfLife.Components
{
    /// <summary>
    /// An abstract class to help read and convert values obtained from the command line
    /// </summary>
    public abstract class PromptBase
    {
        #region Protected Overidable Methods

        /// <summary>
        /// Prompts user with prompt text and returns trimmed clean string
        /// </summary>
        /// <param name="prompt">Message to show user</param>
        /// <returns>Clean string</returns>
        protected virtual string PromptForString(string prompt)
        {
            Write(prompt);
            return ReadLine().Trim();
        }

        /// <summary>
        /// Prompts user with prompt text and converts entered line to Int32
        /// </summary>
        /// <param name="prompt">Message to show user</param>
        /// <returns>Int entered or -1 if conversion failed</returns>
        protected virtual int PromptForInt(string prompt)
        {
            Write(prompt);
            int value = Constants.NULL_INTEGER;
            Int32.TryParse(ReadLine().Trim(), out value);
            return value;
        }

        /// <summary>
        /// Prompts user with prompt text and converts entered line to Boolean
        /// </summary>
        /// <param name="prompt">Message to show user</param>
        /// <returns>Boolean entered or false if conversion failed</returns>
        protected virtual bool PromptForBool(string prompt)
        {
            Write(prompt);
            bool value = false;
            Boolean.TryParse(ReadLine().Trim(), out value);
            return value;
        }

        /// <summary>
        /// Reads the current entered line and converts it to an int.
        /// </summary>
        /// <returns>An int or -1 if conversion failed</returns>
        protected virtual int ReadLineAsInt()
        {
            int option = Constants.NULL_INTEGER;
            Int32.TryParse(ReadLine().Trim(), out option);
            return option;
        }

        #endregion
    }
}