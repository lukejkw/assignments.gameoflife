﻿using GameOfLife.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife.Components
{
    public static class MenuExtensions
    {
        public static MainMenuOptionType ToMenuOption(this int option)
        {
            if (Enum.IsDefined(typeof(MainMenuOptionType), option))
                return (MainMenuOptionType)option;

            return MainMenuOptionType.InvalidOption;
        }

        public static PlayGameOptionType ToPlayGameOption(this int option)
        {
            if (Enum.IsDefined(typeof(MainMenuOptionType), option))
                return (PlayGameOptionType)option;

            return PlayGameOptionType.InvalidOption;
        }
    }
}