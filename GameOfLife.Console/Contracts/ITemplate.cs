﻿using GameOfLife.Common;

namespace GameOfLife.Contracts
{
    public interface ITemplate
    {
        string Name { get; }
        int Height { get; }
        int Width { get; }
        Cell[][] Cells { get; }
    }
}