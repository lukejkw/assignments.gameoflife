﻿using GameOfLife.Common;
using GameOfLife.Contracts;
using System;
using System.Linq;
using System.Text;

namespace GameOfLife
{
    public class GameOfLife : IGameOfLife
    {
        public GameOfLife(int height, int width)
        {
            // Validate params
            if (height <= 0)
                throw new Exception("Height cannot be less than 1");
            if (width <= 0)
                throw new Exception("Width cannot be less than 1");

            // Set Ptops
            Height = height;
            Width = width;

            // Init cell array
            Cell[][] cells = new Cell[height][];
            for (int i = 0; i < height; i++)
            {
                cells[i] = new Cell[width];
                for (int j = 0; j < width; j++)
                {
                    cells[i][j] = Cell.Dead;
                }
            }

            Cells = cells;
        }

        public int Height { get; set; }
        public int Width { get; set; }
        public Cell[][] Cells { get; set; }

        public void InsertTemplate(ITemplate template, int templateX, int templateY)
        {
            if (templateX < 0 || templateX >= Width)
                throw new ArgumentOutOfRangeException("x coord cannot be less than null or greater than the width of the board");
            if (templateY < 0 || templateY >= Height)
                throw new ArgumentOutOfRangeException("x coord cannot be less than null or greater than the width of the board");

            // Keep X value for rest
            int x = templateX;
            foreach (var row in template.Cells)
            {
                // Iterate over
                foreach (Cell cell in row)
                {
                    Cells[templateY][x] = cell;
                    x++;
                }
                templateY++;
                x = templateX;
            }
        }

        public void TakeTurn()
        {
            // Create new cells array to hold next game state
            Cell[][] newCells = new Cell[Height][];
            for (int y = 0; y < Height; y++)
            {
                // Set row of new cells
                newCells[y] = new Cell[Width];
                for (int x = 0; x < Width; x++)
                {
                    // Start counting neighbors
                    int liveNeighborCount = 0;
                    // Check top
                    if (y - 1 >= 0)
                    {
                        // Check top middle
                        if (Cells[y - 1][x] == Cell.Alive)
                            liveNeighborCount++;
                        if (x - 1 >= 0)
                        {
                            // Check top left and left
                            if (Cells[y - 1][x - 1] == Cell.Alive)
                                liveNeighborCount++;
                            if (Cells[y][x - 1] == Cell.Alive)
                                liveNeighborCount++;
                        }
                        if (x + 1 < Width)
                        {
                            // Check top right and right
                            if (Cells[y - 1][x + 1] == Cell.Alive)
                                liveNeighborCount++;
                            if (Cells[y][x + 1] == Cell.Alive)
                                liveNeighborCount++;
                        }
                    }
                    // Check bottom
                    if (y + 1 < Height)
                    {
                        // Check bottom middle
                        if (Cells[y + 1][x] == Cell.Alive)
                            liveNeighborCount++;
                        if (x - 1 >= 0)
                        {
                            // Check bottom left
                            if (Cells[y + 1][x - 1] == Cell.Alive)
                                liveNeighborCount++;
                        }
                        if (x + 1 < Width)
                        {
                            // Check bottom right
                            if (Cells[y + 1][x + 1] == Cell.Alive)
                                liveNeighborCount++;
                        }
                    }

                    bool cellSurvives = false;
                    bool cellAlive = Cells[y][x] == Cell.Alive;

                    // Any live cell with fewer than two live neighbours dies, as if caused by under-population.
                    if (cellAlive && liveNeighborCount < 2)
                        cellSurvives = false;
                    else if (liveNeighborCount == 2 || liveNeighborCount == 3)
                    {
                        // Any live cell with two or three live neighbours lives on to the next generation.
                        // OR
                        // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                        if (cellAlive || (!cellAlive && liveNeighborCount == 3))
                            cellSurvives = true;
                    }
                    // Any live cell with more than three live neighbours dies, as if by over-population.
                    else if (cellAlive && liveNeighborCount > 3)
                        cellSurvives = false;

                    // Set new cells
                    newCells[y][x] = cellSurvives ? Cell.Alive : Cell.Dead;
                }
            }

            // Set new cells
            Cells = newCells;
        }

        /// <summary>
        /// NOTE - For some reason I am getting too many new lines and am failing the tests.
        ///
        /// Ran out of time due to other commitments so could not debug and find issue in time.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder output = new StringBuilder();
            foreach (Cell[] row in Cells)
            {
                foreach (Cell cell in row)
                {
                    output.Append(cell == Cell.Alive
                        ? Constants.ALIVE_CELL_CHAR // Add alive cell char
                        : Constants.DEAD_CELL_CHAR);
                }
                output.AppendLine();
            }
            return output.ToString();
        }
    }
}