﻿using GameOfLife.Common;
using GameOfLife.Contracts;
using GameOfLife.Properties;
using System;
using System.IO;
using System.Linq;
using System.Text;

namespace GameOfLife
{
    public class Template : ITemplate
    {
        public Template(string name, int height, int width, Cell[][] cells)
        {
            // Validate params
            if (String.IsNullOrEmpty(name))
                throw new Exception("Template Name cannot be empty.");
            if (name.Any(c => Path.GetInvalidPathChars().Contains(c)))
                throw new Exception("Template name contains invalid characters.");
            if (height <= 0 || width <= 0)
                throw new Exception("Height/Width less than zero.");
            if (cells == null)
                throw new Exception("Template cell cannot be null");
            if (cells[0].Length != width)
                throw new Exception("Incorrect width parsed");
            if (cells.Length != height)
                throw new Exception("Incorrect height parsed");

            // Set values
            Name = name;
            Height = height;
            Width = width;
            Cells = cells;
        }

        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public Cell[][] Cells { get; set; }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine(Height.ToString());
            sb.AppendLine(Width.ToString());
            foreach (var row in Cells)
            {
                for (int i = 0; i < row.Length; i++)
                {
                    // Append char representation of cell
                    sb.Append(row[i] == Cell.Dead ? Settings.Default.CellDead : Settings.Default.CellAlive);

                    // If last cell, add new line char
                    if (i == row.Length - 1)
                        sb.Append(Environment.NewLine);
                }
            }
            return sb.ToString();
        }
    }
}