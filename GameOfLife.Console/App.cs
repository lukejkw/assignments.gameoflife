﻿using GameOfLife.Properties;
using System;
using static System.Console;

namespace GameOfLife
{
    /// <summary>
    /// Main entry point of console app
    /// </summary>
    public class App
    {
        private static void Main()
        {
            try
            {
                // Start Game
                new Menu().Run();
            }
            catch (Exception ex)
            {
                // Handle any exceptions that may have occured
                WriteLine(Settings.Default.ExceptionOccuredError);
                WriteLine(ex.InnerException);
                Write(Settings.Default.PressAnyKey);
                ReadLine();
            }
        }
    }
}