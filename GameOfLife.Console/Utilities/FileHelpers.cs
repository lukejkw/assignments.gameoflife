﻿using Newtonsoft.Json;
using System.IO;

namespace GameOfLife.Utilities
{
    /// <summary>
    /// A set of methods to help deal with File IO
    /// </summary>
    public static class FileUtils
    {
        public static T GetObjectFromJsonFile<T>(string filePath)
        {
            // Check if file exists
            if (!File.Exists(filePath))
                return default(T);

            // Try deserialize object state
            string json = File.ReadAllText(filePath);
            return JsonConvert.DeserializeObject<T>(json);
        }

        public static void SaveObjectState<T>(this T obj, string filePath)
        {
            // Write all object data to file as json
            string json = JsonConvert.SerializeObject(obj);
            File.WriteAllText(filePath, json);
        }
    }
}