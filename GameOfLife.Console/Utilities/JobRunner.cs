﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife.Utilities
{
    /// <summary>
    /// Job Runner is a utility class that runs tasks on a set interval
    ///
    /// Its whole purpose is to be flexible. It runs any method on an interval.
    ///
    /// Disclaimer - I know that it is probably overkill for this assignment but I got bored and hope to use the code at another time
    /// </summary>
    public class JobRunner
    {
        #region Private Members

        private CancellationToken _cancelToken;
        private TimeSpan _iterationDelay;

        #endregion

        #region Events

        public delegate void JobEvent();

        /// <summary>
        /// Callback before each iteration of the job
        /// </summary>
        public event JobEvent OnBeforeJobIteration;

        /// <summary>
        /// Called for each iteration of the job
        /// </summary>
        public event JobEvent OnJobIteration;

        /// <summary>
        /// Called when the job is canceled/stopped
        /// </summary>
        public event JobEvent OnJobEnd;

        #endregion

        #region Properties

        /// <summary>
        /// Number of times the Job runner has iterated
        /// </summary>
        public int IterationCount { get; private set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Constructs a new job runner
        /// </summary>
        /// <param name="cancelToken">A cancelation token to indicate when the job should stop</param>
        /// <param name="iterationDelay"></param>
        /// <param name="iterationMessage"></param>
        public JobRunner(CancellationToken cancelToken, TimeSpan iterationDelay)
        {
            if (cancelToken == null) throw new ArgumentNullException("Cancel token cannot be null.");
            if (iterationDelay == null) throw new ArgumentNullException("Iteration Delay token cannot be null.");

            _cancelToken = cancelToken;
            _iterationDelay = iterationDelay;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Starts the job runner
        /// </summary>
        /// <returns>Task</returns>
        public async Task StartAsync()
        {
            OnBeforeJobIteration?.Invoke();

            while (!_cancelToken.IsCancellationRequested)
            {
                try
                {
                    await Task.Delay(_iterationDelay, _cancelToken);
                    OnJobIteration?.Invoke();
                    IterationCount++;
                }
                catch { /* Swallow exceptions whole, never to be seen again...*/ }
            }
            OnJobEnd?.Invoke();
        }

        #endregion
    }
}