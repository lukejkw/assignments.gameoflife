﻿using GameOfLife.Common;
using GameOfLife.Components;
using GameOfLife.Properties;
using GameOfLife.Utilities;
using static System.Console;

namespace GameOfLife
{
    /// <summary>
    /// Menu logic and and prompt class
    /// </summary>
    public sealed class Menu : PromptBase
    {
        #region Private Members

        private static TemplateHelper _templateHelper = new TemplateHelper();

        #endregion

        #region Public Methods

        /// <summary>
        /// Main entry point of Game.
        ///
        /// Contains all menu and navigation code
        /// </summary>
        public void Run()
        {
            MainMenuOptionType option = MainMenuOptionType.InvalidOption;
            do
            {
                // Show menu and get option
                ShowMainMenu();
                option = ReadLineAsInt().ToMenuOption();
                switch (option)
                {
                    case MainMenuOptionType.CreateTemplate:
                        CreateTemplate();
                        break;

                    case MainMenuOptionType.PlayGame:
                        PlayGame();
                        break;

                    case MainMenuOptionType.InvalidOption:
                        WriteLine(Settings.Default.InvalidOptionEnteredString);
                        break;
                }
            }
            while (option != MainMenuOptionType.Exit);
            WriteLine("Goodbye!");
        }

        #endregion

        #region Private Helper Methods

        private void ShowMainMenu()
        {
            // Show menu, get option and perform task
            WriteLine(Settings.Default.MainMenuTitle);
            WriteLine(Settings.Default.MainMenuOption1);
            WriteLine(Settings.Default.MainMenuOption2);
            WriteLine(Settings.Default.MainMenuOption3);
            Write(Settings.Default.MainMenuPrompt);
        }

        private void PlayGame()
        {
            // Show menu and get selection
            WriteLine(Settings.Default.PlayGameMenuTitle);
            WriteLine(Settings.Default.PlayGameMenuOption1);
            WriteLine(Settings.Default.PlayGameMenuOption2);
            int selection = PromptForInt(Settings.Default.PlayGameMenuPrompt);
            switch (selection.ToPlayGameOption())
            {
                case PlayGameOptionType.New:
                    NewGameAsync();
                    break;

                case PlayGameOptionType.Resume:
                    ResumeGameAsync();
                    break;

                case PlayGameOptionType.InvalidOption:
                    WriteLine(Settings.Default.InvalidOptionEnteredString);
                    break;
            }
        }

        private void CreateTemplate()
        {
            _templateHelper.CreateTemplate();
        }

        private async void NewGameAsync()
        {
            // Get template selection
            WriteLine(Settings.Default.NewGameTitle);
            Template template = _templateHelper.PromptForTemplate();

            // Check if successful
            if (template == null)
                return;

            // Display to screen and init game
            _templateHelper.DisplayTemplate(template);

            // Init game from template and start
            var game = _templateHelper.InitGameOfLifeFromTemplate(template);
            if (game != null)
                await game?.StartGameAsync();
        }

        private async void ResumeGameAsync()
        {
            GameOfLife game = FileUtils.GetObjectFromJsonFile<GameOfLife>(Settings.Default.SavedGameName);
            if (game == null)
            {
                WriteLine(Settings.Default.ResumeGameNoGameSaved);
                return;
            }

            await game.StartGameAsync();
        }

        #endregion
    }
}