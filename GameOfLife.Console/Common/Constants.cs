﻿namespace GameOfLife.Common
{
    public static class Constants
    {
        public const int NULL_INTEGER = -1;
        public const char DEAD_CELL_CHAR = ' ';
        public const char ALIVE_CELL_CHAR = '\u2588';
    }
}