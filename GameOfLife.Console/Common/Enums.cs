﻿namespace GameOfLife.Common
{
    public enum MainMenuOptionType
    {
        CreateTemplate = 1,
        PlayGame = 2,
        Exit = 3,
        InvalidOption = Constants.NULL_INTEGER
    }

    public enum PlayGameOptionType
    {
        New = 1,
        Resume = 2,
        InvalidOption = Constants.NULL_INTEGER
    }

    public enum Cell
    {
        Alive,
        Dead
    }
}