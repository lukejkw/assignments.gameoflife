# Game of Life Web Application

A simple web application to play the Game of Life.

This application was completed as a university assignment for RMIT University, Web Development Technologies.

## App Breakdown

### 5 Parts

1. Front-end ASP.NET MVC application for front end users
2. Back-end ASP.NET WebForms application for admin users
3. Data Access Layer. This responsible for all data.
4. SQL Layer. All SQL (Tables & Stored Procs) can be found in the Sql proj
5. Business layer. Holds all business logic for application
6. Assignment 1 Console Application and Tests (added so that all code is on one place)

### User Account Notes

 - The front-end ASP.NET MVC application uses Entity Framework throughout as well as OWIN User Authentication. 
 - For simplicity, the same user store is used for both the MVC and WebForms websites.

### DAL Notes

 - A copy of the database can be found in the 'Solution Items' folder 'GameOfLife.bak'. When evaluating the app, I advise that you restore this database to MS SQL Express and change the connection string in both the MVC and WebForms projects
 - The database has been populated with the instructed Admin user account: admin@gmail.com with password 'rmit' (The same user will work with the WebForms website)
 - The EF code uses the Repository & Unit of Work Patterns
 - The ADO.NET code used a simple Repository pattern with "poor man's DI" and Stored Procedured which can be found in the Sql project
 
### Bonus Notes

 - You can upload a template file. The same format as in the first assignment should work
 - Stored Procedures were used for the WebForms project

### Final Things to note

 - I implemented proper OWIN Authentication hence I did not need to encrypt the password manually. Hope that is OK...
 - I copied the home page text from the sample app. I didn't know what else to write here and coming up with new content seemed a waste

## Details of Developer

Luke Warren
s3409172
lukewarrendev.co.za
lukejkw.wordpress.com