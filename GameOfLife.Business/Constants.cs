﻿namespace GameOfLife.Business
{
    public static class Constants
    {
        public const int NULL_INTEGER = -1;
        public const char DEAD_CELL_DISPLAY_CHAR = ' ';
        public const char ALIVE_CELL_DISPLAY_CHAR = '\u2588';
        public const char DEAD_CELL_CHAR = 'X';
        public const char ALIVE_CELL_CHAR = 'O';
    }
}