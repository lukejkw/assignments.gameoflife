﻿using GameOfLife.Data.Domains;
using System;

namespace GameOfLife.Business
{
    public class Utilities
    {
        public static UserTemplate CreateUserGame(string gameName, string userId, byte[] fileBytes)
        {
            var fileContent = System.Text.Encoding.Default.GetString(fileBytes);

            // Split file into lines
            string[] lines = fileContent.Replace("\r\n", ";")
                                        .Split(';'); // Using colon because we can't split on a string

            // Get info from file lines
            var template = new UserTemplate();
            template.Name = gameName; ;
            template.Height = Convert.ToInt32(lines[0]);
            template.Width = Convert.ToInt32(lines[1]);

            // Ensure valid by creating template array
            Cell[][] cells = new Cell[template.Height][];
            for (int i = 0; i < template.Height; i++)
            {
                cells[i] = new Cell[template.Width];
                for (int j = 0; j < template.Width; j++)
                {
                    cells[i][j] = lines[i + 2][j] == Constants.DEAD_CELL_CHAR ? Cell.Dead : Cell.Alive;
                }
            }
            // Could create template array. Convert back to cells string
            template.Cells = cells.ToCellsString();
            template.UserId = userId;
            return template;
        }

        public static string GetDisplayCellsFromDbCells(string dbCells)
        {
            return dbCells.Insert(0, Environment.NewLine) // To avoid weird looking top row
                    .Replace(Constants.DEAD_CELL_CHAR, Constants.DEAD_CELL_DISPLAY_CHAR)
                    .Replace(Constants.ALIVE_CELL_CHAR, Constants.ALIVE_CELL_DISPLAY_CHAR);
        }
    }
}