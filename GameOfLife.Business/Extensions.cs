﻿using GameOfLife.Data.Domains;
using System;
using System.Text;

namespace GameOfLife.Business
{
    /// <summary>
    /// Handy utility methods
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Maps a cells string to a Cell array of arrays
        /// </summary>
        /// <param name="strCells"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static Cell[][] ToCellsArray(this string strCells, int height, int width)
        {
            // Validate params
            if (string.IsNullOrEmpty(strCells))
                throw new ArgumentException("String cannot be null or empty");
            if (height <= 0)
                throw new ArgumentException("Height too small");
            if (width <= 0)
                throw new ArgumentException("Height too small");

            // Make sure that the string at least has enough rows
            string[] rows = strCells.Replace("\r", string.Empty).Split('\n');
            if (rows.Length < height)
                throw new Exception("string does not have the correct number of rows");

            // Map List of string in Cell array of arrays and return
            Cell[][] cells = new Cell[height][];
            for (int i = 0; i < height; i++)
            {
                cells[i] = new Cell[width];
                for (int j = 0; j < width; j++)
                {
                    cells[i][j] = (rows[i][j] == Constants.ALIVE_CELL_CHAR || rows[i][j] == '0')
                        ? Cell.Alive
                        : Cell.Dead;
                }
            }
            return cells;
        }

        /// <summary>
        /// Returns the string representation of a cells array
        /// </summary>
        /// <param name="cells">The cells to use</param>
        /// <returns>string with Environment.NewLine endings</returns>
        public static string ToCellsString(this Cell[][] cells)
        {
            StringBuilder output = new StringBuilder();
            foreach (Cell[] row in cells)
            {
                foreach (Cell cell in row)
                {
                    output.Append(cell == Cell.Alive
                        ? Constants.ALIVE_CELL_CHAR // Add alive cell char
                        : Constants.DEAD_CELL_CHAR);
                }
                output.AppendLine();
            }
            return output.ToString();
        }

        /// <summary>
        /// Inserts a UserTemplate into a user game
        /// </summary>
        /// <param name="game">The UserGame to be used</param>
        /// <param name="template">The UserTemplate to be inserted</param>
        /// <param name="templateX">The X position at which to insert the template</param>
        /// <param name="templateY">The Y position at which to insert the template</param>
        public static void InsertTemplate(this UserGame game, UserTemplate template, int templateX, int templateY)
        {
            // Validate params
            if (template == null) throw new ArgumentException("Template cannot be null");
            if (templateX < 0 || (templateX + template.Width - 1) >= game.Width) throw new ArgumentOutOfRangeException("x coord cannot be less than null or greater than the width of the board");
            if (templateY < 0 || (templateY + template.Height - 1) >= game.Height) throw new ArgumentOutOfRangeException("x coord cannot be less than null or greater than the width of the board");

            // Init cell array for game
            var gameCells = new Cell[game.Height][];
            for (int i = 0; i < game.Height; i++)
            {
                gameCells[i] = new Cell[game.Width];
                for (int j = 0; j < game.Width; j++)
                {
                    gameCells[i][j] = Cell.Dead;
                }
            }

            // Convert cell strings into cell arrays
            var templateCells = template.Cells.ToCellsArray(template.Height, template.Width);

            // Keep X value for rest
            int x = templateX;
            foreach (var row in templateCells)
            {
                // Iterate over
                foreach (Cell cell in row)
                {
                    gameCells[templateY][x] = cell;
                    x++;
                }
                templateY++;
                x = templateX;
            }

            // Set game cells string to new array string
            game.Cells = gameCells.ToCellsString();
        }

        /// <summary>
        /// Ticks the game over one tick
        /// </summary>
        /// <param name="game"></param>
        public static void Tick(this UserGame game)
        {
            // Create new cells array to hold next game state
            Cell[][] newCells = new Cell[game.Height][];
            Cell[][] gameCells = game.Cells.ToCellsArray(game.Height, game.Width);

            for (int y = 0; y < game.Height; y++)
            {
                // Set row of new cells
                newCells[y] = new Cell[game.Width];
                for (int x = 0; x < game.Width; x++)
                {
                    // Start counting neighbors
                    int liveNeighborCount = 0;
                    // Check top
                    if (y - 1 >= 0)
                    {
                        // Check top middle
                        if (gameCells[y - 1][x] == Cell.Alive)
                            liveNeighborCount++;
                        if (x - 1 >= 0)
                        {
                            // Check top left and left
                            if (gameCells[y - 1][x - 1] == Cell.Alive)
                                liveNeighborCount++;
                            if (gameCells[y][x - 1] == Cell.Alive)
                                liveNeighborCount++;
                        }
                        if (x + 1 < game.Width)
                        {
                            // Check top right and right
                            if (gameCells[y - 1][x + 1] == Cell.Alive)
                                liveNeighborCount++;
                            if (gameCells[y][x + 1] == Cell.Alive)
                                liveNeighborCount++;
                        }
                    }
                    // Check bottom
                    if (y + 1 < game.Height)
                    {
                        // Check bottom middle
                        if (gameCells[y + 1][x] == Cell.Alive)
                            liveNeighborCount++;
                        if (x - 1 >= 0)
                        {
                            // Check bottom left
                            if (gameCells[y + 1][x - 1] == Cell.Alive)
                                liveNeighborCount++;
                        }
                        if (x + 1 < game.Width)
                        {
                            // Check bottom right
                            if (gameCells[y + 1][x + 1] == Cell.Alive)
                                liveNeighborCount++;
                        }
                    }

                    bool cellSurvives = false;
                    bool cellAlive = gameCells[y][x] == Cell.Alive;

                    // Any live cell with fewer than two live neighbours dies, as if caused by under-population.
                    if (cellAlive && liveNeighborCount < 2)
                        cellSurvives = false;
                    else if (liveNeighborCount == 2 || liveNeighborCount == 3)
                    {
                        // Any live cell with two or three live neighbours lives on to the next generation.
                        // OR
                        // Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
                        if (cellAlive || (!cellAlive && liveNeighborCount == 3))
                            cellSurvives = true;
                    }
                    // Any live cell with more than three live neighbours dies, as if by over-population.
                    else if (cellAlive && liveNeighborCount > 3)
                        cellSurvives = false;

                    // Set new cells
                    newCells[y][x] = cellSurvives ? Cell.Alive : Cell.Dead;
                }
            }

            // Set new cells
            game.Cells = newCells.ToCellsString();
        }
    }
}