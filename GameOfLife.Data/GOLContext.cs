﻿using GameOfLife.Data.Contracts;
using GameOfLife.Data.Domains;
using GameOfLife.Data.Mappings;
using GameOfLife.Data.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Linq;

namespace GameOfLife.Data
{
    public class GOLContext : IdentityDbContext<ApplicationUser>
    {
        #region Data Sets

        public virtual DbSet<UserTemplate> UserTemplates { get; set; }
        public virtual DbSet<UserGame> UserGames { get; set; }

        #endregion

        #region Overrides

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserGameMapping());
            modelBuilder.Configurations.Add(new UserTemplateMapping());
            modelBuilder.Configurations.Add(new SettingMapping());
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            var changeSet = ChangeTracker.Entries<IAuditable>();
            if (changeSet != null)
            {
                foreach (var entry in changeSet.Where(c => c.State != EntityState.Unchanged))
                {
                    // Update tracking info for auditable objects before save
                    var now = DateTime.Now;
                    if (entry.Entity.CreatedOnDate == DateTime.MinValue
                        || entry.Entity.CreatedOnDate == null)
                        entry.Entity.CreatedOnDate = now;
                    entry.Entity.LastChangedOnDate = now;
                }
            }
            return base.SaveChanges();
        }

        #endregion

        #region Constructors and Initializers

        public GOLContext() : base("DefaultConnection", throwIfV1Schema: false)
        {
            Configuration.LazyLoadingEnabled = true;
        }

        public static GOLContext Create()
        {
            return new GOLContext();
        }

        #endregion
    }
}