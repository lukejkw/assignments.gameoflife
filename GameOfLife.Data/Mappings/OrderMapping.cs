﻿using GameOfLife.Data.Domains;
using System.Data.Entity.ModelConfiguration;

namespace GameOfLife.Data.Mappings
{
    public class UserTemplateMapping : EntityTypeConfiguration<UserTemplate>
    {
        public UserTemplateMapping()
        {
            Property(o => o.Name)
                .IsRequired();
        }
    }
}