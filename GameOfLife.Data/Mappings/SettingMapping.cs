﻿using GameOfLife.Data.Domains;
using System.Data.Entity.ModelConfiguration;

namespace GameOfLife.Data.Mappings
{
    public class SettingMapping : EntityTypeConfiguration<Setting>
    {
        public SettingMapping()
        {
            Property(s => s.Name)
                .HasMaxLength(255)
                .IsRequired();
            Property(s => s.Value)
                .HasMaxLength(255)
                .IsRequired();
        }
    }
}