﻿using GameOfLife.Data.Domains;
using System.Data.Entity.ModelConfiguration;

namespace GameOfLife.Data.Mappings
{
    public class UserGameMapping : EntityTypeConfiguration<UserGame>
    {
        public UserGameMapping()
        {
            Property(i => i.Name)
                .IsRequired();
        }
    }
}