﻿using GameOfLife.Data.Domains;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace GameOfLife.Data.ADO.Repositories
{
    public class UserTemplateRepository : BaseRepository<UserTemplate>
    {
        #region Ctors

        public UserTemplateRepository()
            : base(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        { }

        #endregion

        #region Public Methods

        public IEnumerable<UserTemplate> GetAll()
        {
            using (var command = Connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "UserTemplates_GetAll";
                command.Connection.Open();
                var items = ToList(command);
                command.Connection.Close();
                return items;
            }
        }

        public void Delete(int templateId)
        {
            using (var command = Connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "UserTemplates_Delete";
                command.Parameters.Add(new SqlParameter("Id", templateId));
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
            }
        }

        public void Create(UserTemplate template)
        {
            using (var command = Connection.CreateCommand())
            {
                // Init command
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "UserTemplates_Create";

                // Add params
                command.Parameters.Add(new SqlParameter("Height", template.Height));
                command.Parameters.Add(new SqlParameter("Width", template.Width));
                command.Parameters.Add(new SqlParameter("Cells", template.Cells));
                command.Parameters.Add(new SqlParameter("Name", template.Name));
                command.Parameters.Add(new SqlParameter("UserId", template.UserId));

                // Execute
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
            }
        }

        #endregion

        #region Implemented Methods

        protected override void Map(IDataRecord record, UserTemplate entity)
        {
            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
            entity.Height = (int)record["Height"];
            entity.Width = (int)record["Width"];
            entity.Cells = (string)record["Cells"];
            entity.UserId = (string)record["UserId"];
            entity.CreatedOnDate = (DateTime)record["CreatedOnDate"];
            entity.LastChangedOnDate = (DateTime)record["LastChangedOnDate"];
        }

        protected override UserTemplate CreateEntity()
        {
            return new UserTemplate();
        }

        #endregion
    }
}