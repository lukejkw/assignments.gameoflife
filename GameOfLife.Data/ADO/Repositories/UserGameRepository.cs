﻿using GameOfLife.Data.Domains;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace GameOfLife.Data.ADO.Repositories
{
    public class UserGameRepository : BaseRepository<UserGame>
    {
        #region Ctors

        public UserGameRepository()
            : base(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        { }

        #endregion

        #region Public Methods

        public IEnumerable<UserGame> GetAll()
        {
            using (var command = Connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "UserGames_GetAll";
                command.Connection.Open();
                var items = ToList(command);
                command.Connection.Close();
                return items;
            }
        }

        public void Delete(int id)
        {
            using (var command = Connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "UserGames_Delete";
                command.Parameters.Add(new SqlParameter("Id", id));
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
            }
        }

        #endregion

        #region Implemented Methods

        protected override void Map(IDataRecord record, UserGame entity)
        {
            entity.Id = (int)record["Id"];
            entity.Name = (string)record["Name"];
            entity.Height = (int)record["Height"];
            entity.Width = (int)record["Width"];
            entity.UserId = (string)record["UserId"];
            entity.Cells = (string)record["Cells"];
            entity.CreatedOnDate = (DateTime)record["CreatedOnDate"];
            entity.LastChangedOnDate = (DateTime)record["LastChangedOnDate"];
        }

        protected override UserGame CreateEntity()
        {
            return new UserGame();
        }

        #endregion
    }
}