﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace GameOfLife.Data.ADO.Repositories
{
    public abstract class BaseRepository<TEntity>
    {
        #region Members

        private string _connectionString;
        private SqlConnection _connection;

        protected SqlConnection Connection
        {
            get
            {
                if (_connection == null)
                    _connection = new SqlConnection(_connectionString);
                return _connection;
            }
        }

        #endregion

        #region Ctor

        public BaseRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        protected IEnumerable<TEntity> ToList(IDbCommand command)
        {
            using (var reader = command.ExecuteReader())
            {
                List<TEntity> items = new List<TEntity>();
                while (reader.Read())
                {
                    var item = CreateEntity();
                    Map(reader, item);
                    items.Add(item);
                }
                return items;
            }
        }

        protected abstract void Map(IDataRecord record, TEntity entity);

        protected abstract TEntity CreateEntity();

        #endregion
    }
}