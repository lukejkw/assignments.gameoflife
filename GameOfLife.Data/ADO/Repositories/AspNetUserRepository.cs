﻿using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace GameOfLife.Data.ADO.Repositories
{
    public class AspNetUserRepository : BaseRepository<object>
    {
        #region Ctor

        public AspNetUserRepository() : base(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        { }

        #endregion

        #region Public Methods

        public void DeleteUser(string userId)
        {
            using (var command = Connection.CreateCommand())
            {
                command.CommandType = CommandType.StoredProcedure;
                command.CommandText = "AspNetUsers_Delete";
                command.Parameters.Add(new SqlParameter("UserId", userId));
                command.Connection.Open();
                command.ExecuteNonQuery();
                command.Connection.Close();
            }
        }

        #endregion

        #region BaseRepository Implementation

        protected override object CreateEntity()
        {
            return new object();
        }

        protected override void Map(IDataRecord record, object entity)
        {
            // Do nothing. There is no data retrieval
        }

        #endregion
    }
}