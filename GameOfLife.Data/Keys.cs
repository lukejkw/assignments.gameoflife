﻿namespace GameOfLife.Data
{
    /// <summary>
    /// Keys when checking for roles
    /// </summary>
    public class RoleKeys
    {
        public const string ADMIN = "Administrator";
    }
}