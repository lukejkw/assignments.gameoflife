﻿using GameOfLife.Data.Domains;

namespace GameOfLife.Data.Contracts
{
    public interface IUserGameRepository : IRepository<UserGame>
    {
    }
}