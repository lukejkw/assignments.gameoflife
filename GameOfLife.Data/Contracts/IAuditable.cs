﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife.Data.Contracts
{
    public interface IAuditable
    {
        DateTime LastChangedOnDate { get; set; }
        DateTime CreatedOnDate { get; set; }
    }
}