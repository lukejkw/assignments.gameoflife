﻿using GameOfLife.Data.Domains;

namespace GameOfLife.Data.Contracts
{
    public interface IUserTemplateRepository : IRepository<UserTemplate>
    {
    }
}