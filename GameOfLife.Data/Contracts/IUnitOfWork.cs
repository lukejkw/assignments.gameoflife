﻿using System;

namespace GameOfLife.Data.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
        IUserTemplateRepository UserTemplates { get; }
        IUserGameRepository UserGames { get; }

        int Complete();
    }
}