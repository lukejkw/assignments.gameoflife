﻿using GameOfLife.Data.Contracts;
using GameOfLife.Data.Domains;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace GameOfLife.Data.Repositories
{
    public class UserGameRepository : RepositoryBase<UserGame>, IUserGameRepository
    {
        private DbSet<UserGame> _entities;

        public UserGameRepository(DbContext context) : base(context)
        {
            _entities = context.Set<UserGame>();
        }

        public override void AddOrUpdate(UserGame entity)
        {
            _entities.AddOrUpdate(entity);
        }
    }
}