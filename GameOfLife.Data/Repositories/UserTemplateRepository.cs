﻿using GameOfLife.Data.Contracts;
using GameOfLife.Data.Domains;
using System.Data.Entity;
using System.Data.Entity.Migrations;

namespace GameOfLife.Data.Repositories
{
    public class UserTemplateRepository : RepositoryBase<UserTemplate>, IUserTemplateRepository
    {
        private DbSet<UserTemplate> _entities;

        public UserTemplateRepository(GOLContext context) : base(context)
        {
            _entities = context.Set<UserTemplate>();
        }

        public override void AddOrUpdate(UserTemplate entity)
        {
            _entities.AddOrUpdate(entity);
        }
    }
}