﻿using GameOfLife.Data;
using GameOfLife.Data.Contracts;
using GameOfLife.Data.Repositories;
using System;

namespace GameOfLife
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public IUserTemplateRepository UserTemplates { get; private set; }
        public IUserGameRepository UserGames { get; private set; }

        private readonly GOLContext _context;

        public UnitOfWork(GOLContext context)
        {
            _context = context;
            UserTemplates = new UserTemplateRepository(_context);
            UserGames = new UserGameRepository(_context);
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}