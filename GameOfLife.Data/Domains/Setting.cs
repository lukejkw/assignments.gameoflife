﻿namespace GameOfLife.Data.Domains
{
    public class Setting : AuditableBase
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}