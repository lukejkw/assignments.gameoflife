﻿using GameOfLife.Data.Security;

namespace GameOfLife.Data.Domains
{
    public class UserGame : AuditableBase
    {
        #region Properties

        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public string Cells { get; set; }
        public string UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        #endregion

        #region Ctor

        public UserGame()
        {
        }

        #endregion
    }
}