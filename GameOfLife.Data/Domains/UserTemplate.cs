﻿using GameOfLife.Data.Security;

namespace GameOfLife.Data.Domains
{
    public class UserTemplate : AuditableBase
    {
        #region Properties

        public string UserId { get; set; }
        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public string Cells { get; set; }

        public virtual ApplicationUser User { get; set; }

        #endregion

        #region Ctor

        public UserTemplate()
            : base()
        {
        }

        #endregion
    }
}