﻿using GameOfLife.Data.Contracts;
using System;

namespace GameOfLife.Data.Domains
{
    public abstract class AuditableBase : DomainBase, IAuditable
    {
        public DateTime CreatedOnDate { get; set; }

        public DateTime LastChangedOnDate { get; set; }

        public AuditableBase()
        {
            CreatedOnDate = DateTime.MinValue;
            LastChangedOnDate = DateTime.MinValue;
        }
    }
}