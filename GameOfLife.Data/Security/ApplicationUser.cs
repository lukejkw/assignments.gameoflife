﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace GameOfLife.Data.Security
{
    public class ApplicationUser : IdentityUser
    {
        #region Custom Profile Properties

        public string First { get; set; }
        public string Last { get; set; }

        #endregion

        #region Read Only

        public string FullName
        {
            get
            {
                return $"{First} {Last}";
            }
        }

        #endregion

        #region Methods

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }

        #endregion

        #region Ctors

        public ApplicationUser() : base()
        {
        }

        #endregion
    }
}