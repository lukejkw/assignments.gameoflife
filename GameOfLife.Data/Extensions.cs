﻿using GameOfLife.Data.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Security.Principal;

namespace GameOfLife.Data
{
    public static class Extensions
    {
        /// <summary>
        /// Returns an application user from a IPrincipal User
        /// </summary>
        /// <param name="user">The IPrincipal user</param>
        /// <returns>The application user or null</returns>
        public static ApplicationUser ToApplicationUser(this IPrincipal user)
        {
            var userPrincipal = (ClaimsPrincipal)user;
            if (userPrincipal.Identity.IsAuthenticated)
            {
                var userStore = new UserStore<ApplicationUser>(GOLContext.Create());
                var userManager = new ApplicationUserManager(userStore);

                return userManager.FindById(userPrincipal.Identity.GetUserId());
            }
            return null;
        }
    }
}