﻿<%@ Page Title="Login" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="GameOfLife.WebForms.Login" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <h1>Admin Login
        </h1>
        <p>
            NB - Only an admin users can login to the back-office
        </p>
        <div>
            <div class="form-group">
                <label>Email</label>
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" />
                <asp:RequiredFieldValidator ID="rfvEmail" ControlToValidate="txtEmail" ForeColor="Red" runat="server" Text="You must enter an email address" />
            </div>
            <div class="form-group">
                <label>Username</label>
                <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="form-control" />
                <asp:RequiredFieldValidator ID="rfvPassword" ControlToValidate="txtPassword" ForeColor="Red" runat="server" Text="You must enter an password" />
            </div>
            <asp:LinkButton ID="cmdLogin" CssClass="btn btn-primary" runat="server">
                Login
            </asp:LinkButton>
        </div>
    </div>
</asp:Content>