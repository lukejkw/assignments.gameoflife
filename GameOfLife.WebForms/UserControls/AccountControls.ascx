﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountControls.ascx.cs" Inherits="GameOfLife.WebForms.UserControls.AccountControls" %>

<ul class="nav navbar-nav navbar-right">
    <li id="liUserName" runat="server">
        <a href="?">
            <asp:Label ID="lblUserName" runat="server" />
        </a>
    </li>
    <li id="liLogOff" runat="server">
        <asp:LinkButton ID="cmdLogOff" runat="server">
            Logoff
        </asp:LinkButton>
    </li>
    <li id="liLogin" runat="server">
        <asp:HyperLink ID="lnkLogin" href="../Login.aspx" runat="server">
            Login
        </asp:HyperLink>
    </li>
</ul>