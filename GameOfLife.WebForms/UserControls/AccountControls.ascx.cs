﻿using Microsoft.AspNet.Identity;
using System;
using System.Web;
using System.Web.UI;

namespace GameOfLife.WebForms.UserControls
{
    public partial class AccountControls : UserControl
    {
        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            cmdLogOff.Click += CmdLogOff_Click;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated && HttpContext.Current.User != null)
            {
                // User Logged in
                liUserName.Visible = true;
                liLogOff.Visible = true;
                liLogin.Visible = false;
                lblUserName.Text = HttpContext.Current.User.Identity.Name;
            }
            else
            {
                // User logged out
                liUserName.Visible = false;
                liLogOff.Visible = false;
                liLogin.Visible = true;
            }
        }

        private void CmdLogOff_Click(object sender, EventArgs e)
        {
            if (Request.IsAuthenticated && HttpContext.Current.User != null)
            {
                // Log the user off if they are logged in
                HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                Response.Redirect("/login", false);
            }
        }

        #endregion
    }
}