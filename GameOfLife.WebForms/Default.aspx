﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="GameOfLife.WebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Site Users
    </h1>
    <asp:Repeater ID="rptUsers" DataSourceID="odsUsers" runat="server">
        <HeaderTemplate>
            <table class="table">
                <thead>
                    <tr>
                        <th>First
                        </th>
                        <th>Last
                        </th>
                        <th>Email
                        </th>
                        <th>UserName
                        </th>
                        <th>Delete
                        </th>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <tr>
                    <td>
                        <%#Eval("First") %>
                    </td>
                    <td>
                        <%#Eval("Last") %>
                    </td>
                    <td>
                        <%#Eval("Email") %>
                    </td>
                    <td>
                        <%#Eval("UserName") %>
                    </td>
                    <td>
                        <asp:LinkButton ID="cmdDelete" CommandName="DELETE" CommandArgument='<%#Eval("Id") %>' OnClick="cmdDelete_Click" runat="server">
                            Delete User
                        </asp:LinkButton>
                    </td>
                </tr>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <asp:ObjectDataSource ID="odsUsers"
        TypeName="GameOfLife.Data.Security.ApplicationUserManager, GameOfLife.Data"
        SelectMethod="GetAll"
        runat="server" />
</asp:Content>