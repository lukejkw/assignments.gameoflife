﻿<%@ Page Title="Templates" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Templates.aspx.cs" Inherits="GameOfLife.WebForms.Templates" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <a class="btn btn-default" href="UploadTemplate.aspx">Upload Template
        </a>
    </div>

    <asp:repeater id="rptTemplates" datasourceid="odsTemplates" runat="server">
        <HeaderTemplate>
            <table class="table">
                <thead>
                    <tr>
                        <th>Name
                        </th>
                        <th>Width
                        </th>
                        <th>Height
                        </th>
                        <th>Cells
                        </th>
                        <th>Created
                        </th>
                        <th>Actions
                        </th>
                    </tr>
                </thead>
        </HeaderTemplate>
        <ItemTemplate>
            <tbody>
                <tr>
                    <td>
                        <%#Eval("Name") %>
                    </td>
                    <td>
                        <%#Eval("Width") %>
                    </td>
                    <td>
                        <%#Eval("Height") %>
                    </td>
                    <td>
                        <pre style="display:inline-block;">
                            <%# GameOfLife.Business.Utilities.GetDisplayCellsFromDbCells(Eval("Cells").ToString())%>
                        </pre>
                    </td>
                    <td>
                        <%#Convert.ToDateTime(Eval("CreatedOnDate")).ToString("D") %>
                    </td>
                    <td>
                        <asp:LinkButton ID="cmdDelete" CommandName="DELETE" CommandArgument='<%#Eval("Id") %>' OnClick="LinkButton_Click" runat="server">
                            Delete
                        </asp:LinkButton>
                    </td>
                </tr>
            </tbody>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:repeater>
    <asp:objectdatasource id="odsTemplates"
        typename="GameOfLife.Data.ADO.Repositories.UserTemplateRepository, GameOfLife.Data"
        selectmethod="GetAll"
        runat="server" />
</asp:Content>