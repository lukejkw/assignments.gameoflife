﻿using GameOfLife.Business;
using GameOfLife.Data.ADO.Repositories;
using GameOfLife.WebForms.Components;
using System;
using System.Web.UI.WebControls;

namespace GameOfLife.WebForms
{
    public partial class Templates : PageBase
    {
        #region Members

        private UserTemplateRepository _repo = new UserTemplateRepository();

        #endregion

        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            // Attach event handlers
            odsTemplates.ObjectCreating += OdsTemplates_ObjectCreating;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
            }
            catch (Exception ex)
            {
                WriteError(ex);
            }
        }

        private void OdsTemplates_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            try
            {
                // Init ODS instance reference
                e.ObjectInstance = _repo;
            }
            catch (Exception ex)
            {
                WriteError(ex);
            }
        }

        protected void LinkButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Cast button so we can access attributes
                var button = sender as LinkButton;
                if (button != null)
                {
                    // Try get id from button
                    int id = Constants.NULL_INTEGER;
                    if (Int32.TryParse(button.CommandArgument, out id))
                    {
                        // See what action needs to be taken and do it
                        switch (button.CommandName)
                        {
                            case "DELETE":
                                // Delete template and rebind repeater
                                _repo.Delete(id);
                                rptTemplates.DataBind();
                                break;

                            case "UPLOAD":
                                // Navigate to upload page
                                Response.Redirect("~/UploadTemplate.aspx", false);
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteError(ex);
            }
        }

        #endregion
    }
}