﻿<%@ Page Title="Upload Template" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="UploadTemplate.aspx.cs" Inherits="GameOfLife.WebForms.UploadTemplate" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="container">
        <h1>Upload a Template</h1>
        <div class="form-group">
            <label>Select a file to upload</label>
            <asp:FileUpload ID="ctlTemplateUpload" runat="server" />
        </div>
        <asp:LinkButton ID="cmdUpload" CssClass="btn btn-primary" runat="server">
            Upload File
        </asp:LinkButton>
        <a class="btn btn-default" href="Templates.aspx">Back to Templates
        </a>
    </div>
</asp:Content>