﻿using System;
using System.Web.UI;

namespace GameOfLife.WebForms.Components
{
    public abstract class PageBase : Page
    {
        #region Methods

        /// <summary>
        /// Writes the exception as Bootstrap UI alert to screen
        /// </summary>
        /// <param name="ex">The exception to write</param>
        protected void WriteError(Exception ex)
        {
            WriteError(ex.Message);
        }

        protected void WriteError(string message)
        {
            Response.Write($"<div class\"container\"><div class=\"alert alert-danger\">{message}</div></div>");
        }

        protected void WriteSuccess(string message)
        {
            Response.Write($"<div class\"container\"><div class=\"alert alert-success\">{message}</div></div>");
        }

        #endregion
    }
}