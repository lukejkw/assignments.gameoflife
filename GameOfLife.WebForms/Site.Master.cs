﻿using System;
using System.Web;
using System.Web.UI;

namespace GameOfLife.WebForms
{
    public partial class SiteMaster : MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Ensure that logged out users are always taken to the login page
            if (!Request.IsAuthenticated && !Request.Url.LocalPath.Contains("/login"))
                Response.Redirect("/login.aspx", false);
        }
    }
}