﻿using GameOfLife.Data.ADO.Repositories;
using GameOfLife.Data.Domains;
using GameOfLife.Data.Security;
using GameOfLife.WebForms.Components;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace GameOfLife.WebForms
{
    public partial class _Default : PageBase
    {
        protected void cmdDelete_Click(object sender, EventArgs e)
        {
            try
            {
                // Get LinkButton
                var button = sender as LinkButton;
                if (button != null)
                {
                    // Get Id of user
                    string userId = button.CommandArgument;
                    // Delete user
                    new AspNetUserRepository().DeleteUser(userId);
                    rptUsers.DataBind();
                }
            }
            catch (Exception ex)
            {
                WriteError(ex);
            }
        }
    }
}