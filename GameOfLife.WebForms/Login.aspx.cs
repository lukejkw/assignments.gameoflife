﻿using GameOfLife.Data;
using GameOfLife.Data.Security;
using GameOfLife.WebForms.Components;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Web;

namespace GameOfLife.WebForms
{
    public partial class Login : PageBase
    {
        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            try
            {
                // If user is already logged in,
                // redirect them away from the login page
                // Ensure that logged out users are always taken to the login page
                if (Request.IsAuthenticated)
                    Response.Redirect("/default", false);
            }
            catch (Exception ex)
            {
                WriteError(ex);
            }
            cmdLogin.Click += CmdLogin_Click;
        }

        private void CmdLogin_Click(object sender, EventArgs e)
        {
            try
            {
                // Get values and cleanup whitespace
                string email = txtEmail.Text.Trim();
                string password = txtPassword.Text.Trim();

                // Ensure we have values
                if (String.IsNullOrEmpty(email) || String.IsNullOrEmpty(password))
                {
                    WriteError("You must enter both a username and password");
                    return;
                }

                // Try get user
                var userManager = HttpContext.Current.GetOwinContext().Get<ApplicationUserManager>();
                var user = userManager.Users.FirstOrDefault(u => u.Email == email);
                if (user == null)
                {
                    WriteError("Email or password incorrect");
                    return;
                }

                // See if user in admin role
                var roles = userManager.GetRoles(user.Id);
                if (roles == null || roles.Count == 0 || !roles.Contains(RoleKeys.ADMIN))
                {
                    // User is not an admin, log them out and show message
                    HttpContext.Current.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                    WriteError("You have an account but you are not an admin");
                }

                // Sign the user in
                var signinManager = HttpContext.Current.GetOwinContext().Get<ApplicationSignInManager>();
                var result = signinManager.PasswordSignInAsync(email, password, false, shouldLockout: false).Result;
                switch (result)
                {
                    case SignInStatus.Success:
                        // Redirect to home page
                        Response.Redirect("/Default.aspx", false);
                        break;

                    case SignInStatus.Failure:
                    default:
                        WriteError("Email or password incorrect");
                        break;
                }
            }
            catch (Exception ex)
            {
                WriteError(ex);
            }
        }

        #endregion
    }
}