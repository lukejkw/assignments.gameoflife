﻿using GameOfLife.Business;
using GameOfLife.Data;
using GameOfLife.Data.ADO.Repositories;
using GameOfLife.Data.Domains;
using GameOfLife.WebForms.Components;
using System;
using System.IO;

namespace GameOfLife.WebForms
{
    public partial class UploadTemplate : PageBase
    {
        #region Events

        protected void Page_Init(object sender, EventArgs e)
        {
            cmdUpload.Click += CmdUpload_Click;
        }

        private void CmdUpload_Click(object sender, EventArgs e)
        {
            try
            {
                // Validate Upload
                if (!ctlTemplateUpload.HasFiles)
                {
                    WriteError("You must select a file!");
                    return;
                }
                if (!ctlTemplateUpload.FileName.Contains(".txt"))
                {
                    WriteError("Sorry but we only accept .txt files");
                    return;
                }

                // Get content of file
                byte[] bytes = ctlTemplateUpload.FileBytes;
                string gameName = Path.GetFileNameWithoutExtension(ctlTemplateUpload.FileName);
                string userId = User.ToApplicationUser().Id;
                UserTemplate template = Utilities.CreateUserGame(gameName, userId, bytes);

                // Create template
                var repo = new UserTemplateRepository();
                repo.Create(template);

                WriteSuccess($"Successfully created template '{template.Name}'");
            }
            catch (IndexOutOfRangeException)
            {
                WriteError("Could not create template. Incorrect template format");
            }
            catch (Exception ex)
            {
                WriteError(ex);
            }
        }

        #endregion
    }
}